#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 19 22:26:19 2019

@author: broz
"""

from bokeh.plotting import figure, show
import numpy as np

p = figure(title="simple line example", x_axis_label='x', y_axis_label='y')

k = 2
omega = 0.1
t = 0
x = np.arange(-2*np.pi,2*np.pi, 4*np.pi/1000)
y = np.sin(k*x - omega*t)

p.line(x,y)

show(p)
