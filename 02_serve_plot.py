#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 19 22:26:19 2019

@author: broz
"""

from bokeh.plotting import figure, show, curdoc
import numpy as np

p = figure(title="simple line example", x_axis_label='x', y_axis_label='y')

k = 2
omega = 0.1
t = 0
x = np.arange(-2*np.pi,2*np.pi, 4*np.pi/1000)
y = np.sin(k*x - omega*t)

p.line(x,y)


if __name__ == "__main__":
    show(p)
else:
    #This section is called, if this script is not called directly. In this
    #case the plot is prepared to be used from a webserver. You can start a
    #corresponding web server, serving this small applet for example with the
    #following command:
    #bokeh serve 03_animate_plot.py --allow-websocket-origin=pi4b.local:5006
    curdoc().add_root(p)
