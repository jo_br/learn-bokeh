#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 19 22:26:19 2019

@author: broz

Demonstrates the usage od a DataSource for bokeh
"""

from bokeh.plotting import figure, curdoc, show
from bokeh.models import ColumnDataSource
import numpy as np


# create the data
k = 2
OMEGA = 0.1
t = 0
x = np.arange(-2*np.pi, 2*np.pi, 4*np.pi/1000)

data = {'x_values':x,
        'y_values':np.sin(k*x - OMEGA*t)}
source = ColumnDataSource(data=data)

# plot the data from the data source
p = figure(title="simple line example", x_axis_label='x', y_axis_label='y')
p.line(x='x_values', y='y_values', source=source)


# serve the plot
if __name__ == "__main__":
    show(p)
else:
    curdoc().add_root(p)
