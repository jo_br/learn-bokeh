#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 19 22:26:19 2019

@author: broz

This is the first example for updating plots. We use streaming to create an
animated plot.

The plot ist first configured, to display time series data from a
CollumnDataSource.


"""

from bokeh.plotting import figure, curdoc
from bokeh.models import ColumnDataSource
from bokeh.driving import count
import numpy as np
import time

# create the data
omega = 1*(2*np.pi) # omega = 2*pi*f
              
data = {'t_values':[0],
        'y_values':[0]}
source = ColumnDataSource(data=data)

# declare and configure the plot to display the data from the CDS
p = figure(title="simple line example", x_axis_label='t', y_axis_label='y')
p.line(x='t_values', y='y_values', source=source)

# This function pushes new data into the CDS. Redisplay is invoked
# automatically
time_at_start = time.time()
@count()
def update(cnt):
    # acquire new values and push them into the ColumnDataSource.
    t = time.time() - time_at_start     
    new_data = {'t_values':[t],
            'y_values':[np.sin(omega*t)]}
    source.stream(new_data,rollover=100)
    # the streasm command pushes in new data into the data source.
    # The rollover parameter sets the maximum size of the CDS.

# serve the plot
curdoc().add_root(p)
time_at_start = time.time()
curdoc().add_periodic_callback(update, 1000/30)


# You can start the server with the command
# bokeh serve 03_animate_plot.py --allow-websocket-origin=pi4b.local:5006

