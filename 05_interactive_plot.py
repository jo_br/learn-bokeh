#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 19 22:26:19 2019

@author: broz

In this example, a interactive gui is used to display some plot.

The GUI shall consist of a axis grid, showing a plot for a sine function.
Sliders allow to modify offset, amplitude, frequency and phase.

"""
from bokeh.layouts import row, column
from bokeh.plotting import figure, curdoc, show 
from bokeh.models import ColumnDataSource, Slider
from bokeh.driving import count
from bokeh.models.widgets import Div

import numpy as np

# create the sliders
ofst_slider = Slider(title='offset',
                     start=-10, end=10, step=0.1,
                     value=0)
amp_slider = Slider(title='amplitude',
                    start=-10, end=10, step=0.1,
                    value=1)
frq_slider = Slider(title='frequency',
                    start=-0.01, end=2, step=0.01,
                    value=1)
phs_slider =   Slider(title='phase',
                      start=-10, end=10, step=0.1,
                      value=1) 

# define a function, reading out the values of the sliders, and returning the
#data to be displayed
def get_data(*args):
    c0 = ofst_slider.value
    a = amp_slider.value
    f = frq_slider.value
    phi = phs_slider.value
    
    x = np.arange(-10, 10, 0.01)
    y = c0 + a*np.sin(2*np.pi*f*x + phi)
    
    return {'x_values':x, 'y_values':y}


data_source = ColumnDataSource(data=get_data())

# defining the data creation function.
# This function takes the settings of the parameters and updates the data in
# in the data source           
f = figure(x_range=(-10,10), y_range=(-10,10), plot_width=400, plot_height=400)
f.line(x='x_values', y='y_values', source=data_source)

# layouting the content
# create a colum with the sliders
slider_column = column(ofst_slider, amp_slider, frq_slider, phs_slider)
# place this slider in a row, next to the plot
main_row = row(f, slider_column)

# final layout, by adding a header
pre = Div(text="<h1>Frequency Generator</h1>")
layout = column(pre, main_row)

# defining the action, when a slider is moved
def update(attr, old, new):
    data_source.data = get_data()

ofst_slider.on_change('value', update)
amp_slider.on_change('value', update)
frq_slider.on_change('value', update)
phs_slider.on_change('value', update)
# and link to the sliders                


# startup client, depending on server mode or pythen interpreter mode
if __name__=='__main__':
    server = Server({'/': bkapp}, num_procs=4)
    server.start()
    
else:
    curdoc().add_root(layout)


# Assume you want to use the computer with the name pi4b in the local network.
# You can start the server with the command
# bokeh serve 03_animate_plot.py --allow-websocket-origin=pi4b.local:5006

