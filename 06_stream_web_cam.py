#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 21 12:55:15 2019

@author: broz

Uses bokeh and OpenCV to stream a webcam image
"""

from bokeh.plotting import figure, curdoc
from bokeh.models import ColumnDataSource
from bokeh.driving import count
import time
import cv2

cap = cv2.VideoCapture(0)
ret, frame = cap.read()
frame = cv2.flip(frame, 0)
rgba = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
h, w, d = rgba.shape

p = figure(plot_width=900, plot_height=int(900/w*h))
p.x_range.range_padding = p.y_range.range_padding = 0
im = p.image_rgba([rgba], x=0, y=0, dw=w, dh=h)

@count()
def update(cnt):
    """
    gets a new image from the camera and updates the data source
    """
    # acquire new values and push them into the ColumnDataSource.
    ret, frame = cap.read()
    frame = cv2.flip(frame, 0)
    # Our operations on the frame come here
    rgba = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
    new_data = {'image':[rgba]}
    im.data_source.data = new_data
    # the streasm command pushes in new data into the data source.
    # The rollover parameter sets the maximum size of the CDS.
    #cv2.waitKey(1)

# serve the plot
curdoc().add_root(p)
time_at_start = time.time()
curdoc().add_periodic_callback(update, 1000)
