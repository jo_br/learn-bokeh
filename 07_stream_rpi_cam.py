#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 21 12:55:15 2019

@author: broz

Uses bokeh to stream images from raspberrypi camera
"""
from bokeh.plotting import figure, curdoc
from bokeh.driving import count
import numpy as np
import picamera

# this will set up a figure area of the size w, h with an image
# glyph for rendering the image_buffer.
# The image buffer will be updated by the picamera and
#sceduled for redisplay by placing into the image glyphs data
# source

# First: set up the figure object to draw into
w, h =(640, 480)
p = figure(plot_width=w, plot_height=int(h))
p.x_range.range_padding = p.y_range.range_padding = 0

# Second: set up the image_buffer and pass over to an image_rgba
# glyph object as the data_source. Note, that image_rgba is
# building the data source object automatically around the
# image_buffer 
image_buffer = np.empty((h, w, 4,), dtype=np.uint8)
im = p.image_rgba([image_buffer], x=0, y=0, dw=w, dh=h)

# Third: initiate the camera object
camera = picamera.PiCamera()
camera.resolution = (w, h)
camera.vflip=True


@count()
def update(cnt):
    """
    gets a new image from the camera and updates the data source
    """
    camera.capture(image_buffer, 'rgba')
    new_data = {'image':[image_buffer]}
    im.data_source.data = new_data


# serve the plot
curdoc().add_root(p)
curdoc().add_periodic_callback(update, 10)
