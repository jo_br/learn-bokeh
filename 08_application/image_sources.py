#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb  2 10:50:16 2020

@author: broz

Classes to setup image processing pipelines. Includes modifications, importers
and exporters.
"""


import bokeh
from bokeh.plotting import figure, curdoc, show
from bokeh.models import ColumnDataSource, RangeSlider, Slider
from bokeh.driving import count
from bokeh.layouts import Column, Row
import time
import cv2
import logging
import numpy as np
import glob


def adjust_gamma(image, gamma=1.0):
    if type(image) == type(None):
        return None
    max_val = np.iinfo(image.dtype).max    
    invGamma = 1.0 / gamma
    
    return np.floor(((image / max_val) ** invGamma) * max_val).astype(image.dtype)


class ImageSource(object):
    def __init__(self):
        super(ImageSource, self).__init__()
        print('ImageSource: initializing...')
        self.im = None   # the source image
        self.callbacks = []
        print('ImageSource: ...done')
    
    def add_callback(self, a_listener):
        if not a_listener in self.callbacks:
            self.callbacks.append(a_listener)
    
    def remove_callback(self, a_listener):
        if a_listener in self.callbacks:
            self.callbacks.remove(a_listener)
    
    def invoke_callbacks(self, msg):
        for callback in self.callbacks:
            callback(msg, None, None)
    
    def set_im(self, a_image):
        self.im = a_image
        self.invoke_callbacks('image_update')


class ImageConsumer(object):
    def __init__(self):
        super(ImageConsumer, self).__init__()
        print('ImageConsumer: initializing ...')
        self.source = None
        print('ImageConsumer: ...done')
        
    def connect(self, image_source:ImageSource):
        self.disconnect()
        self.source = image_source
        self.source.add_callback(self.get_update)
        
    def disconnect(self):
        if not type(self.source) is type(None):
            self.source.remove_callback(self.get_update)
            self.source = None
        
    def get_update(self, attr, old, new):
        self.process_image(self.source.im)
            
    def process_image(self, im):
        Warning('abstract method called, please implement process_image')
        return im.copy()


class ImageProcessor(ImageSource, ImageConsumer):
    def __init__(self):
        print('ImageProcessor: initializing...')
        super(ImageProcessor, self).__init__()
        super(ImageProcessor, self).__init__()
        print('ImageProcessor: ... done')
        

class ImageImporter(ImageSource):
    def __init__(self, *args):
        super(ImageImporter, self).__init__()
        self.iterator = None
        if len(args)==1:
            self.open(args[0])
        
    def open(self, a_pattern):
        self.pattern = a_pattern
        self.iterator = iter(sorted(glob.glob(self.pattern)))
        self.import_next()
        
    def import_next(self):
        next_image_path = next(self.iterator, None)
        if not next_image_path:
            self.iterator = iter(sorted(glob.glob(self.pattern)))
            next_image_path = next(self.iterator, None)
        if next_image_path:
            self.set_im(cv2.imread(next_image_path, cv2.IMREAD_UNCHANGED))
        else:
            raise Exception('No image')


    
class GammaFilter(ImageProcessor):
    def __init__(self, source:ImageSource):
        super(GammaFilter, self).__init__()
        self.gamma = 1.0
        self.min = 0
        self.max = 1
        super(GammaFilter, self).connect(source)
        self.stretch_slider = RangeSlider(start=0, end=1, step = 0.01, value=[self.min, self.max],title="Strech")
        self.stretch_slider.on_change('value_throttled', self.get_update)
        self.stretch_slider.callback_policy = "mouseup"
        self.gamma_slider = Slider(start=0.1, end=10, value=self.gamma,step=0.1, title="Gamma")
        self.gamma_slider.callback_policy = "mouseup"
        self.gamma_slider.on_change('value_throttled', self.get_update)
        self.control_component = Column(self.gamma_slider, self.stretch_slider)
            
        
    def get_update(self, attr, old, new):
        self.gamma = self.gamma_slider.value
        self.min, self.max = self.stretch_slider.value
        super(GammaFilter, self).get_update(attr, old, new)



    def set_gamma(self, gamma):
        self.gamma = gamma
        self.gamma_slider.value = self.gamma
        self.stretch_slider.value = (self.min, self.max)
        self.get_update('ParameterUpdate', None, None)
    
    def process_image(self, im):
        if type(im) == type(None):
            return None
        imtype = im.dtype
        im = adjust_gamma(im, gamma=self.gamma)
        max_val = np.iinfo(im.dtype).max
        max_pix = max_val * self.max
        min_pix = max_val * self.min
        im = (im - min_pix)/(max_pix - min_pix)
        im[im<0]=0
        im[im>1]=1
        im = np.floor( im*max_val ).astype(imtype)
        self.set_im(im)


class StarDetector(ImageProcessor):
    def __init__(self):
        super(StarDetector, self).__init__()
        self.keypoints=[]
        self.params = cv2.SimpleBlobDetector_Params()
        self.params.minArea = 15
        self.params.maxArea =10000
        self.params.blobColor=255
        self.params.thresholdStep=1
        self.params.minThreshold=0
        self.params.maxThreshold=255
        self.params.minRepeatability=2
        self.params.filterByConvexity=False
        
        self.threshold_slider = RangeSlider(
            start=0, end=255,
            value=[self.params.minThreshold, self.params.maxThreshold],
            title='Thresholds',
            callback_policy = "mouseup")
        
        self.area_slider = RangeSlider(
            start=0, end=1000,
            value=[self.params.minArea, self.params.maxArea],
            title='area',
            callback_policy = "mouseup")
        
        self.threshold_slider.on_change("value_throttled", self.update)
        self.area_slider.on_change("value_throttled", self.update)
    
    
    def update(self, attr, old, new):
        thresholds = self.threshold_slider.value
        self.params.minThreshold = thresholds[0];
        self.params.maxThreshold = thresholds[1];
        
        area = self.area_slider.value
        self.params.minArea = area[0]
        self.params.maxArea = area[1]
        
        self.process_image(self.source.im)
        
        
    def process_image(self, im):
        im = cv2.cvtColor(im.copy(), cv2.COLOR_RGB2GRAY)
        im = (im-np.min(im))/np.max(im)
        mean = cv2.GaussianBlur(im,(255,255),0)
        detect = cv2.GaussianBlur(im,(5,5),0)
        blob= (detect > (2*mean+0*np.sqrt(mean)))
        blob = np.uint8(blob)*255
        
        detector = cv2.SimpleBlobDetector_create(self.params)
        self.keypoints = detector.detect(blob)
        print(len(self.keypoints))
        im_with_keypoints = cv2.drawKeypoints(
                np.uint8(im*255),
                self.keypoints,
                np.array([]),
                (255,200,200),
                cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        self.set_im(im_with_keypoints)


class Cv2Camera(ImageSource):
    def __init__(self, devide):
        self.open(0)
    
    def open(self, device):
        self.cap = cv2.VideoCapture(device)
        self.update()

    def import_next(self):
        if not self.cap.isOpened():
            return
        
        ret, frame = self.cap.read()
        if ret:
            frame = cv2.flip(frame, 0)
            self.im = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
            self.invoke_callbacks('image_update')

    
    def close(self):
        self.cap.release()
        
    
    
class ImageView(ImageConsumer):
    """
    Provides a view on a image source.
    The view must update if the source is updated.
    The view must adapt its aspect ratio according to the source image
    The view must be configurable by providing the number of rows
    The view must display a black image if not connected to an source
    The view must be able to be unconnected from its source
    The view must provide a method for replacing its source
    """
    def __init__(self, dh:int):
        """
        creates a view with the height dh pixels

        Parameters
        ----------
        dh : int
            display height in pixels .

        Returns
        -------
        ImageView

        """
        super(ImageConsumer, self).__init__()
        # crearing the figure element which carries the image
        self.fig = figure()
        self.fig.x_range.range_padding = 0
        self.fig.y_range.range_padding = 0
        self.fig.plot_height = dh
        self.fig.plot_width = int(640/480*dh)
        self.source = None
        
        im = np.zeros((480,640,4),dtype='uint8')
        im[:,:,3] = 255
        self.glyph = self.fig.image_rgba(
            image=[im],
            x=0,
            y=0,
            dw = self.fig.plot_height,
            dh = self.fig.plot_height)
        

    def process_image(self, imaga_data):     
        """
        push image to screen.
        Writes the image data into its data source image storage

        Parameters
        ----------
        imaga_data : TYPE
            the image.

        Raises
        ------
        TypeError
            DESCRIPTION.

        Returns
        -------
        None.

        """
        # figure out if we deal with 'uint16'
        if type(imaga_data) == type(None):
            return None
        im = imaga_data.copy()
        if im.dtype == np.dtype('uint16'):
            im = np.array(im/256.).astype('uint8')
        
        # figure out the depth of the image
        if len(im.shape)==2:
            # image is monocrome
            im = cv2.cvtColor(im, cv2.COLOR_GRAY2RGBA)
        elif len(im.shape)==3:
            if im.shape[2]==3:
                im = cv2.cvtColor(im, cv2.COLOR_RGB2RGBA)
            elif im.shape[2]!=4:
                raise TypeError('image format unknown')
        else:
            raise TypeError('image format unknown')
        
        # now we have an rbga image. Next step is to updat drawing
        # width
        dh, dw, c = im.shape
        self.fig.plot_width = int(dw * self.fig.plot_height/dh)
        self.glyph.data_source.data = {'image':[im]}
