#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 21 12:55:15 2019

@author: broz

Uses bokeh and OpenCV to stream a webcam image
"""
import bokeh
from bokeh.plotting import figure, curdoc, show
from bokeh.models import ColumnDataSource, RangeSlider
from bokeh.driving import count
from bokeh.layouts import Column, Row
import time
import cv2
import logging
import numpy as np
import glob

from image_sources import StarDetector, GammaFilter, ImageImporter, ImageView

#test_image = 'resources/ASICAP_2019-03-29_22_14_34_313.tiff'
test_image = 'resources/2019-08-22-2058_1-M13_0316.tiff'
#test_image = 'resources/2019-12-04-2032_4-M27_0045.tiff'
#test_image = '/home/pi/Pictures/*.png'
   

source = ImageImporter(test_image)
view = ImageView(480)
view.connect(source)

star_detector = StarDetector()
star_detector.connect(source)

view2 = ImageView(480)
view2.connect(star_detector)


def update():
    source.import_next()
    
curdoc().add_root(Column(view.fig,
                         star_detector.threshold_slider,
                         star_detector.area_slider,
                         view2.fig))
update()
#curdoc().add_periodic_callback(update, 1000/25)