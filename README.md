# Learn Bokeh

a collection of some examples, intended to understand how bokeh works.

The Examples can be run by invoking the individual modules with

`python 01_show_plot.py`

or by setting up a running bokeh server accessable from your local machine

`bokeh serve 04_animate_plot.py --show`

If you want, the server to be accessible over a network, you have to allow
websocket origin to be from an accessable adress of the machine running the
server. For example

`bokeh serve 04_streaming_plot.py --allow-websocket-origin="$(hostname | tr '[:upper:]' '[:lower:]'):5006"`

makes the server freely accessable within your local network.